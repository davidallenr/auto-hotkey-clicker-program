﻿/*


        Configuration File for Connection AHK



*/
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Global InstallDir           := "C:\Users\opeio\AppData\Local\FiveM"                     ; Location of FiveM install
Global ExeName              := "FiveM.exe"                                              ; Your FiveM Exe Name
Global DelayBeforeStart     := 10
Global TimeDelayClose       := 5                                                        ; Delay In seconds before clicking Close
Global TimeDelay            := 2                                                        ; Delay In seconds before clicking bar or connect
Global ServerIP             := "144.217.69.165:29934"
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~