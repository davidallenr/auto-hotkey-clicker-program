﻿#SingleInstance, Force
#Include %A_ScriptDir%/tools/config.ahk
SetTitleMatchMode 1 
SetTitleMatchMode Fast  
Global xLocationArray 	:= []
Global yLocationArray 	:= []		
Global ArraySize		:= 0
Global GameID           := "Not Set Yet"
Global GameName         := "Not Set Yet"
Global IsComplete       := 0
Global IsComplete2      := 0
Global GlobStatus       :=
Global ArrayTotal       := 
Global TotalLoop        :=
Global LisPressed       := 0

MainLoop:
    Loop
        {
        
        StoreWindowLocation()
            {
                i 			:=  0
                j           :=  0
                LisPressed 	:=  0
                ArrayLoop   :=  0
                 
                GuiControl ,,StatusUpdate, Left click on a window to Store X/Y Locations. Right Click to Stop.
                Loop %ArraySize% 
                    {
                        xLocationArray[j] := Null
                        yLocationArray[j] := Null
                        j++
                    }
                Loop 
                    {   
                        MouseGetPos, tempX, tempY, 
                        GetKeyState, LeftMouse, LButton
                        GetKeyState, RightMouse, RButton
                        LeftMouse 	    :=  GetKeyState("LButton")
                        RightMouse      :=  GetKeyState("RButton")
                        Tooltip, X: %tempX% Y: %tempY% %ArrayTotal%
                        ArrayTotal      :=  (xLocationArray.MaxIndex()+1)
                        
                        if(RightMouse==True)
                            {
                                Tooltip,
                                ArraySize   := (i - 1)
                                IsComplete  := 1
                                GuiControl ,,StatusUpdate,
                                Gosub FillWindow
                            }
                            Else If (%IsComplete% == 1)
                                {
                                    Gosub FillWindow
                                }
                        if(LeftMouse==False)
                            {
                                LisPressed := 1
                            }
                        if(LisPressed==1)
                            {
                                if(LeftMouse==True)
                                    {
                                        MouseGetPos, tempX, tempY
                                        xLocationArray[i] 	:= tempX
                                        yLocationArray[i]	:= tempY
                                        i++ 
                                        LisPressed           := 0
                                    }
                            }
                    }
            }
            
        FillWIndow:
            {
                ToolTip,
                GuiControl ,,StatusUpdate, Adding Click Locations To List
                sleep, 10
                i           := 0
                l           := 0
                ArrayFill   := 0
                Loop
                    {
                        if (i == 0)
                            {
                                GuiControl,, ListFill,
                                Loop, %ArrayTotal%
                                    {
                                    i++
                                    ToolTip, Make into LOOP  I= %i% L= %l% Array Total: %ArrayTotal% %A_Index% 
                                    ;sleep, 2000
                                    If (%ArrayTotal% == %i% )
                                        {
                                        ToolTip, I'm updating on a loop!
                                        GuiControl ,,StatusUpdate, Adding Click Locations.
                                        sleep, 40
                                        LV_Add("", "X" . xLocationArray[(A_Index-1)],"Y" . yLocationArray[(A_Index-1)])     ; Takes the Array_Index of the loop and subtracts 1
                                        if(%ArrayTotal% > A_Index)
                                            {
                                                ToolTip, Finished Adding Click Locations.
                                                Sleep, 5000
                                                Gosub MainLoop
                                            }
                                        }
                                    }
                                if(RightMouse==True)
                                    {
                                        ToolTip, 
                                        sleep, 5000
                                        Tooltip, Make it into Break
                                    }    
                            }   
                    }
            }
        return

        StoreWindowInfo()
            {    
                i := 0
                LisPressed2 	:= 0
                GuiControl ,,StatusUpdate, Left click on the active window to update the Process ID and Title. Right Click to Stop.                       
                Loop 
                {
                    GetKeyState, LeftMouse, LButton
                    GetKeyState, RightMouse, RButton
                    LeftMouse 	:= GetKeyState("LButton")
                    RightMouse  := GetKeyState("RButton")
                        if(LisPressed2 == 1)                                                ;JINK: LI2 = Variable changed from first function to prevent conflicts
                            {
                            if(LeftMouse==True)
                                {
                                    If (i == 0)
                                        {
                                            WinGet, tempID, PID, A
                                            WinGetTitle, tempName , A
                                            GameID              := tempID
                                            GameName            := tempName
                                            GuiControl ,,EGameID, %GameID%
                                            GuiControl ,,EGameName, %GameName%
                                            i++                                             ;JINK: i = Counter 0 once it reaches 1 it is reset so the button can be clicked again and reset. 
                                        }
                                        Else if (i == 1)
                                        {
                                            ToolTip, 
                                            SLEEP, 2000
                                            i   := 0
                                            Break
                                        }
                                }
                            }
                        if(LeftMouse==False)
                        {
                                LisPressed2 := 1
                        }
                        if(RightMouse==True)                                                ;JINK: Right Mouse Still Exits Function if no id.
                            {
                                Tooltip,
                                IsComplete2  := 1
                                GuiControl ,,StatusUpdate,
                                Break
                            }
                }
            }

        Connect()
            {	
                ArrayCompare        := 0
                tempCycles          := 0
                i                   := 0
                NotFilled           := 1
                
                Loop
                    {	
                        if (%ArrayCompare% == 0)
                            {
                                ClickX          := xLocationArray[i]
                                ClickY          := yLocationArray[i]
                                ;ToolTip, It's into the Array Compare Portion. %ClickX%
                                Sleep, 1000
                                MouseMove, %clickX%, %ClickY%, 40                                   ;JINK: Mouse Move is for local Testing.
                                ;ControlClick,, ahk_exe FiveM.exe,,LEFT,, NA x%ClickX% y%ClickY%    ;JINK: Sends Clicks to the EXE of the PID gathered In the click. 
                                i++
                                If ((xLocationArray.MaxIndex()+1) == i)
                                    {
                                        tempCycles++
                                        ;ToolTip, Resetting The I
                                        SLeep 2000
                                        i := 0
                                        GuiControl ,,StatusUpdate, Cycles Ran: %tempCycles%
                                    } 
                                    Else If ((xLocationArray.MaxIndex()+1) < i)
                                    {   
                                        Gui Font, Bold
                                        GuiControl ,,StatusUpdate, Nothing to connect to.
                                        Sleep, 1000
                                        GuiControl ,,StatusUpdate,
                                    } 
                                 
                            }
                    }
                Loop
                    {
                        tempCycles++
                    }

            }
            
        CloseWindow()
            {   
                loop 1
                    {
                        NavToConnection     :=  0
                        ifWinExist (ahk_pid %GameID%)
                        {
                            Process, Close, %GameID%
                        }
                    }    
            }
            
        OpenWindow()
            {
                loop 1
                {
                    NavToConnection     :=  0
                    IfWinNotExist (ahk_pid %GameID%)
                    {
                        sleep, 1000
                        Run, %ExeName%, %InstallDir% 
                    } 
                }
            }
            
        ResetScript()
            {   Loop, 1
                    {
                        Global xLocationArray 	:= []
                        Global yLocationArray 	:= []		
                        Global ArraySize		:= 0
                        Global GameID           := "Not Set Yet"
                        Global GameName         := "Not Set Yet"
                        Global IsComplete       := 0
                        Global GlobStatus       :=
                        Global ArrayTotal       := 
                        Global TotalLoop        :=
                        Global LisPressed       := 0
                        Break
                    }
            }
        }

 