﻿/*
	Program Written By	: 		Jink Left
	Date Started		: 		03 June 2017
	Date Of Last Edit	:		12 June 2017
	
	Program description	:
	This program is an all in one for connecting to a FiveM Server and is Fully Configureable.
*/
#NoEnv
#SingleInstance, Force
#Include %A_ScriptDir%/tools/functions.ahk	
SetWorkingDir %A_ScriptDir%

^q::
    ;SetTimer, Progress, 2000
    Gui +AlwaysOnTop
        Gui Add, Text, x-88 y-24 w691 h0 0x10
        Gui Add, GroupBox, x112 y49 w193 h42, Window Name
        Gui Add, GroupBox, x112 y96 w194 h42, Process ID
        Gui Add, GroupBox, x448 y48 w174 h159, Wait Time In Seconds
        Gui Add, CheckBox, x536 y72 w64 h23, Enable
        Gui Add, CheckBox, x536 y96 w64 h23, Enable
    
    Gui Font, Bold
        Gui Add, Button, gConnect x8 y8 w95 h35, Connect
    Gui Font, Norm
        Gui Add, Button, gOpenWindow x8 y88 w95 h35, Open Game
        Gui Add, Button, gCloseWindow x8 y128 w95 h35, Close Game
        Gui Add, Button, gPauseButton x8 y168 w95 h35, Pause Script
        Gui Add, Button, gStoreWindowLocation x319 y14 w120 h35, Get Click Locations
        Gui Add, Button, gStoreWindowInfo x112 y8 w121 h35, Get Process ID
        Gui Add, Button, gResetScript x360 y216 w80 h23, Reset Script
        Gui Add, Button, x448 y216 w80 h23, Save Config
        Gui Add, Button, x536 y216 w80 h23, Load Config

        Gui Add, Edit, vEGameName x120 y64 w180 h21 +ReadOnly, %GameID%
        Gui Add, Edit, vEGameID x120 y112 w180 h21  +ReadOnly, %GameName%
        Gui Add, Edit, x456 y99 w77 h21 +Number +Limit2
        Gui Add, Edit, x456 y75 w77 h21 +Number +Limit2
    Gui Font, Bold
        Gui Add, ListView,vListFill x320 y56 w120 h153 +NoSortHdr, X Loc| Y Loc|
    Gui Font, Norm
        Gui Add, StatusBar, vStatusUpdate , 
    Gui Show, w630 h270, Jink's AIO Script
Return
    

PauseButton:
    Pause, Toggle
    Return

GuiEscape:
GuiClose:
    ExitApp
	
^z::
	Pause, Toggle
    
    

